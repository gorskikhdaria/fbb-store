var bookStore = angular.module('bookStore', ['ui.router']);

bookStore.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                abstract: true,
                template: '<header></header>'
            })
            .state('app.booklist', {
                url: '/',
                template: '<booklist></booklist>'
            })
            .state('app.about', {
                url: '/about',
                template: '<about></about>'
            })
            .state('app.book', {
                url: '/{bookId}',
                template: '<book></book>'
            })
            .state('app.purchase', {
                url: '/{bookId}/purchase',
                template: '<purchase></purchase>'
            });
        $urlRouterProvider.otherwise('/');
    }
]);