module.exports = function(grunt) {
	require("load-grunt-tasks")(grunt);

	var sassFolderPath = 'styles';

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				files: {
					'css/main.css': 'styles/main.sass'
				}
			}
		},
		ftp_push: {
			demo: {
				options: {
					authKey: 'netology',
					host: 'university.netology.ru',
					dest: '/fbb-store/',
					port: 21
				},
				files: [{
					expand: true,
					cwd: '.',
					src: [
						'index.html',
						'css/main.css'
					]
				}]
			}
		},
        autoprefixer:{
            dist:{
                files:{
                    'css/main.css':'css/main.css'
                }
            }
        },
		watch: {
			sass: {
				files: ['styles/*.sass'],
				tasks: ['sass']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-ftp-push');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['sass', 'ftp_push', 'watch']);
	grunt.registerTask('start', ['sass', 'autoprefixer']);
	//grunt.registerTask('start', ['autoprefixer']);
};