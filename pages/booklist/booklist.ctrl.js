function BookListCtrl($scope, $sce, BooksSrv, $rootScope, $timeout) {
    // Drag and drop для книг
    $timeout(function() {
        var bookWrap = $('.booklist__book-wrap');
        $('.booklist').sortable({
            items: '> .booklist__book-wrap',
            zIndex: 500,
            start: function() {
                bookWrap.css('cursor', 'move');
            },
            stop: function() {
                bookWrap.css('cursor', 'auto');
            }
        });
    }, 1000);

    // Настройки для ng-repeat
    $scope.limit = 4;
    $scope.begin = 1;
    // Делаем видимым прелоудеры и невидимыми ненужные элементы
    $scope.bookListLoading = true;

    // Загружаем список книг
    BooksSrv.loadBookList().then(function(data) {
        $scope.bookList = data;
        $scope.sce = $sce;
        $scope.bookListLoading = false;
    });

    // Логика кнопки Еще несколько
    $scope.showMore = function() {
        var index = 1;
        $scope.limit = $scope.limit*(index + 1);
        index++;
        if ($scope.limit < $scope.bookList.length) {
            $('.booklist__more-button')[0].style.display = 'none';
        }
    };

    // Подписываемся на событие ввода текста для поиска в header
    $rootScope.$on('searchUpdate', function(e, data) {
        $scope.search = data;
    });
}

bookStore.controller('BookListCtrl', BookListCtrl)
    .component('booklist', {
        templateUrl: './pages/booklist/booklist.html',
        controller: BookListCtrl
    });