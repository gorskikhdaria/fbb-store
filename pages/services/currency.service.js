function CurrencySrv($http) {
    var currencyList;

    this.loadCurrencyList = function() {
        return $http({
            method: 'GET',
            url: 'https://netology-fbb-store-api.herokuapp.com/currency',
            cache: true
        })
            .then(function(data) {
                currencyList = data.data;
                return currencyList;
            }, function(error, status) {
                console.log('Произошла ошибка при загрузке курса валют' + status + ': ' + error);
            });
    };

    this.getCurrencyList = function() {
        return currencyList;
    };

    this.getCurrencyByCharCode = function(charCode) {
        return currencyList.find(function(item) {
            return item.CharCode === charCode;
        });
    };

    this.getCurrencyCodeById = function(id) {
        return currencyList.find(function(curr) {
            return curr.ID === id;
        });
    };

    this.calcPrice = function(price, fromCurrency, toCurrency) {
        return Math.ceil(price * fromCurrency.Value / toCurrency.Value);
    };
}

bookStore.service('CurrencySrv', CurrencySrv);