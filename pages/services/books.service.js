function BooksSrv($http) {
    this.loadBookList = function() {
        return $http({
            method: 'GET',
            url: 'https://netology-fbb-store-api.herokuapp.com/book',
            cache: true
        })
        .then(function(data) {
            return data.data;
        }, function(error, status) {
            console.log('Произошла ошибка ' + status + ': ' + error);
        });
    };


    this.loadBookById = function(bookId) {
        return $http({
            method: 'GET',
            url: 'https://netology-fbb-store-api.herokuapp.com/book/' + bookId,
            cache: true
        })
            .then(function(data) {
                return data.data;
            }, function(error, status) {
                console.log('Произошла ошибка ' + status + ': ' + error);
            });
    };
}

bookStore.service('BooksSrv', BooksSrv);