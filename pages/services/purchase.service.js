function PurschaseSrv($http) {
    this.loadDeliveryMethods = function() {
        return $http({
            method: 'GET',
            url: 'https://netology-fbb-store-api.herokuapp.com/order/delivery',
            cache: true
        })
        .then(function(data) {
            return data.data;
        }, function(error, status) {
            console.log('Произошла ошибка при загрузке методов доставки ' + status + ': ' + error);
        });
    };

    this.loadPaymentMethods = function() {
        return $http({
            method: 'GET',
            url: 'https://netology-fbb-store-api.herokuapp.com/order/payment',
            cache: true
        })
        .then(function(data) {
            return data.data;
        }, function(error, status) {
            console.log('Произошла ошибка при загрузке метода оплаты' + status + ': ' + error);
        });
    };

    this.sendOrder = function(data) {
        return $http.post('https://netology-fbb-store-api.herokuapp.com/order', data).then(function(response) {
            return response;
        });
    };

    // this.getDeliveryMethods = function() {
    //     return this.deliveryMethods;
    // };
    //
    // this.getPaymentMethods = function() {
    //     return this.paymentMethods;
    // };
}

bookStore.service('PurschaseSrv', PurschaseSrv);
