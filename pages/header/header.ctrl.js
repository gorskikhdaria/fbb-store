function HeaderCtrl($scope, $state, $rootScope, CurrencySrv) {
    // Список валют в select
    CurrencySrv.loadCurrencyList().then(function(data){
        $scope.currencyList = data;
        $scope.currentCurrency = CurrencySrv.getCurrencyByCharCode('USD');

        // Событие изменения валюты
        $scope.$watch('currentCurrency', function(newVal, prevVal) {
            $rootScope.currentCurrency = newVal || CurrencySrv.getCurrencyByCharCode('USD');
            $rootScope.prevCurrency = prevVal || CurrencySrv.getCurrencyByCharCode('USD');
            $rootScope.$broadcast('changeCurrency');
        });
    });

    // Изменить курсор на стейте app.about
    $rootScope.$on('$stateChangeSuccess',
        function(){
            if ($state.is('app.about')) {
                $('.about-wrap').css('cursor', 'default');
            }
    });

    // Спрятать текст placeholder-а при клике
    $scope.onBlurInput = function() {
        if ($('.search-input').prop('value') === '') {
            $scope.hideSearchText = false;
        }
    };

    // Переход на страницу О компании
    $scope.goToAbout = function() {
        if (!$state.is('app.about')) {
            $state.go('app.about');
        }
    };

    // Вызываем событие для движения зрачка глаза на странице книги
    $scope.onMouseMove = function(e) {
        if ($state.is('app.book')) {
            $rootScope.$broadcast('pupilMove', e);
        }
    };

    // Вызвать событие ввода текста в поиск
    $scope.$watch('search', function(newVal) {
        $rootScope.$broadcast('searchUpdate', newVal);
    });
}

bookStore.controller('HeaderCtrl', HeaderCtrl)
    .component('header', {
        templateUrl: './pages/header/header.html',
        controller: HeaderCtrl
    });
