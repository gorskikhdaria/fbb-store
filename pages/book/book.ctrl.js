function BookCtrl($scope, $sce, BooksSrv, $stateParams, CurrencySrv, $rootScope) {
    // Показываем прелодер
    $scope.bookLoading = true;
    // Загружаем книгу
    BooksSrv.loadBookById($stateParams.bookId).then(function(data) {
        // Загружаем список валют
        CurrencySrv.loadCurrencyList().then(function() {
            $scope.sce = $sce;
            $scope.book = data;

            // Текущее значение валюты
            $scope.currentCurrency = $rootScope.currentCurrency || CurrencySrv.getCurrencyByCharCode('USD');
            $scope.prevCurrency = $rootScope.prevCurrency || CurrencySrv.getCurrencyByCharCode('USD');

            $scope.bookPrice = CurrencySrv.calcPrice($scope.book.price, $scope.prevCurrency, $scope.currentCurrency);

            // Событие при смене валюты
            $scope.$on('changeCurrency', function() {
                $scope.currentCurrency = $rootScope.currentCurrency;
                $scope.prevCurrency = $rootScope.prevCurrency;
                $scope.bookPrice = CurrencySrv.calcPrice($scope.book.price, $scope.prevCurrency, $scope.currentCurrency);
            });

            $scope.bookLoading = false;
        });
    });

    // Функция для движения зрачка глаза за курсором мыши
    $rootScope.$on('pupilMove', function(event, data) {
        var e = data;
        var pupil = $('.book__eye-pupil');
        var pupilWrap = $('.book__eye-wrap');

        // Координаты курсора относительно центра глаза
        var positionX = e.pageX - pupil.offset().left;
        var positionY = pupil.offset().top - e.pageY;

        // Косинус угла между курсором и осью X
        var cos = Math.abs(positionX / Math.sqrt(Math.pow(positionX, 2) + Math.pow(positionY, 2)));

        // Считем расстояние от центра глаза до зрачка, оно же - гипотенуза
        // С помомщью текущей дистанции от курсора до центра глаза
        var currentDistance = Math.sqrt(Math.pow(positionX, 2) + Math.pow(positionY, 2));

        // И макс. дитанции от курсора до центра глаза
        var maxDistance = Math.sqrt(Math.pow(pupilWrap.offset().left + 35, 2) + Math.pow(pupilWrap.offset().top + 35, 2));
        var eyeDistance = ( 25 * currentDistance ) / maxDistance;

        // Считаем координаты текущего положения зрачка, относит. центра глаза
        var offsetX = cos * eyeDistance;
        var offsetY = Math.sqrt(Math.pow(eyeDistance, 2) - Math.pow(offsetX, 2));
        var resultX;
        var resultY;

        if (positionX > 0 && positionY > 0) {
            resultX = 31 + offsetX;
            resultY = 31 - offsetY;
        } else if (positionX > 0 && positionY < 0) {
            resultX = 31 + offsetX;
            resultY = 31 + offsetY;
        } else if (positionX < 0 && positionY < 0) {
            resultX = 31 - offsetX;
            resultY = 31 + offsetY;
        } else if (positionX < 0 && positionY > 0) {
            resultX = 31 - offsetX;
            resultY = 31 - offsetY;
        }
        pupil.css('top', resultY + 'px');
        pupil.css('left', resultX + 'px');
    });

    // Сдеалть закладку в книге
    $scope.toggleRibbon = function() {
        var ribbon = $('.book__ribbon');
        if (ribbon.css('background-image').match('ribbon-grey')) {
            ribbon.css('background-image', 'url(../imgs/ribbon-green.png)');
        } else {
            ribbon.css('background-image', 'url(../imgs/ribbon-grey.png)');
        }
    };
}

bookStore.controller('BookCtrl', BookCtrl)
    .component('book', {
        templateUrl: './pages/book/book.html',
        controller: BookCtrl
    }
);

