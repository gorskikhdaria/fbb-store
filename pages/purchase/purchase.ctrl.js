function PurschaseCtrl($scope, $sce, PurschaseSrv, $stateParams, BooksSrv, CurrencySrv, $rootScope) {
    // Показываем прелоудер, скрываем ненужное
    $scope.purchaseLoading = true;
    $scope.purchaseSuccess = false;
    $scope.purchaseError = false;
    BooksSrv.loadBookById($stateParams.bookId).then(function(data) {
        $scope.sce = $sce;
        $scope.book = data;
        $scope.purchaseLoading = false;
    });

    // Загружаем методы доставки, затем для каждого метода ищем код валюты
    PurschaseSrv.loadDeliveryMethods().then(function(data) {
        // Загружаем список валют
        CurrencySrv.loadCurrencyList().then(function() {
            $scope.delMethods = data;

            // Нужно ли показывать поле для адреса
            $scope.checkNeedAddress = function(e) {
                var currentDelivery = $scope.delMethods.find(function(item) {
                    return item.id === e.target.value;
                });
                $scope.needAdress = currentDelivery.needAdress;
            };

            // Достаем текущее значение валюты
            $scope.currentCurrency = $rootScope.currentCurrency || CurrencySrv.getCurrencyByCharCode('USD');
            $scope.prevCurrency = $rootScope.prevCurrency || CurrencySrv.getCurrencyByCharCode('USD');
            $scope.bookPrice = CurrencySrv.calcPrice($scope.book.price, $scope.prevCurrency, $scope.currentCurrency);
            $scope.sum = $scope.bookPrice;

            // Функция для расчета
            calcDeliveryPrice = function(){
                $scope.delMethods.forEach(function(item) {
                    item.convertedPrice = CurrencySrv.calcPrice(item.price, $scope.prevCurrency, $scope.currentCurrency);
                });
            };

            calcSum = function(convertedDeliveryPrice) {
                $scope.sum = $scope.bookPrice + convertedDeliveryPrice;
            };

            calcDeliveryPrice();

            $scope.calcSum = function(convertedDeliveryPrice) {
                calcSum(convertedDeliveryPrice);
            };

            // Событие при смене валюты
            $scope.$on('changeCurrency', function() {
                $scope.currentCurrency = $rootScope.currentCurrency;
                $scope.prevCurrency = $rootScope.prevCurrency;
                calcDeliveryPrice();

                if ($scope.order === undefined) {
                    $scope.sum = $scope.bookPrice;
                } else {
                    var currentDeliveryPrice = $scope.delMethods.find(function(item) {
                        return item.id = $scope.order.deliveryId;
                    });
                    calcSum(currentDeliveryPrice.convertedPrice);
                }
            });
        });
    });

    // Загружаем методы оплаты
    PurschaseSrv.loadPaymentMethods().then(function(paymentList) {
        $scope.payMethods = paymentList;
        // $scope.payMethods = availPayMethods || paymentList;

        $scope.giveSelectedDelivery = function(delivery) {
            var availPayMethods = [];
            paymentList.forEach(function(item) {
                if (item.availableFor.indexOf(delivery.id) !== -1) {
                    availPayMethods.push(item);
                }
                $scope.payMethods = availPayMethods;
            });
        };
    });

    // Отправляем заказ
    $scope.sendOrder = function(order) {

        $(document).ready(function(){
            $scope.purchaseSending = true;
        });

        var data = {
            manager: 'gorskih.darya@yandex.ru',
            book: $scope.book.id,
            name: order.name,
            phone: order.phone,
            email: order.email,
            comment: order.comment,
            delivery: {
                id: order.deliveryId,
                address: order.address
            },
            payment: {
                id: order.paymentId,
                currency: $scope.book.currency
            }
        };

        PurschaseSrv.sendOrder(data).then(function(resp) {
            $scope.purchaseSending = false;
            $scope.purchaseSended = true;
            if (resp.status === 200) {
                $scope.purchaseSuccess = true;
            } else {
                $scope.purchaseError = true;
            }
        });
    };
}

bookStore.controller('PurschaseCtrl', PurschaseCtrl)
    .component('purchase', {
        templateUrl: './pages/purchase/purchase.html',
        controller: PurschaseCtrl
    });
